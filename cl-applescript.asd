(defpackage #:cl-applescript-system
  (:use :cl :asdf))

(in-package #:cl-applescript-system)


(defsystem cl-applescript
  :author "Daniel Dickison <danieldickison@gmail.com>"
  :version "1.0"
  :depends-on ("flexi-streams")
  :serial t
  :components ((:file "package")
	       (:file "unportable")
	       (:file "portable")))
