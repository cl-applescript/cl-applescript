(defpackage #:cl-applescript
  (:use :cl :flexi-streams)
  (:export #:run-applescript
	   #:parse-applescript-output
	   #:native-namestring
	   #:*applescript-symbol-package*)
  (:documentation "Provides a portable interface to running AppleScript scripts on Mac OS X 10.5 and later systems."))
