(in-package #:cl-applescript)


(defun native-namestring (path)
  "Returns a string representation of `path` that can be passed to OS API's."
  #+:clozure (namestring (translate-logical-pathname path))
  #+:sbcl (sb-ext:native-namestring path))


(defun spawn-thread (function
		     &key (name "itunes-puppeteer"))
  "Creates a new thread that runs `function`."
  #+:clozure (ccl:process-run-function name function)
  #+:sbcl (sb-thread:make-thread function :name name))


(defun kill-thread (thread)
  "Kills `thread` if it's not dead already."
  #+:clozure (ccl:process-kill thread)
  #+:sbcl (when (sb-thread:thread-alive-p thread)
	    (sb-thread:terminate-thread)))


(defun run-program (program args
		    &key
		    input output error wait
		    (sharing :private)
		    (element-type 'character))
  "Runs an external program as a sub-process.  See CCL's documentation of `ccl:run-program` which is the model for this function."
  #+:clozure (ccl:run-program program args
			      :sharing sharing
			      :element-type element-type
			      :wait wait
			      :input input
			      :output output
			      :error error)
  #+:sbcl (sb-ext:run-program program args
			      :search t
			      :wait wait
			      :input input
			      :output output
			      :error error))


(defun process-output-stream (proc)
  "Gets the output stream associated with the external program process `proc`."
  #+:clozure (ccl:external-process-output-stream proc)
  #+:sbcl (sb-ext:process-output proc))

(defun process-input-stream (proc)
  "Gets the input stream associated with the external program process `proc`."
  #+:clozure (ccl:external-process-input-stream proc)
  #+:sbcl (sb-ext:process-input proc))

(defun process-error-stream (proc)
  "Gets the error stream associated with the external program process `proc`."
  #+:clozure (ccl:external-process-error-stream proc)
  #+:sbcl (sb-ext:process-error proc))
